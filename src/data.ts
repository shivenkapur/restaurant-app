import { ItemData } from './interfaces/index';

const data: ItemData[] = [
  {
    productId: 1,
    options: ['small', 'medium', 'large'],
    title: 'Product 1',
    content: 'This is Product 1',
    price: 10,
  },
  {
    productId: 2,
    options: ['strawberry', 'apple', 'guava'],
    title: 'Product 2',
    content: 'This is Product 2',
    price: 20,
  },
  {
    productId: 3,
    options: ['mango', 'orange', 'kiwi'],
    title: 'Product 3',
    content: 'This is Product 3',
    price: 30,
  },
  {
    productId: 4,
    options: ['cool', 'cooler', 'coolest'],
    title: 'Product 4',
    content: 'This is Product 4',
    price: 40,
  },
  {
    productId: 5,
    options: ['cool', 'cooler', 'coolest'],
    title: 'Product 5',
    content: 'This is Product 5',
    price: 50,
  },
  {
    productId: 6,
    options: ['cool', 'cooler', 'coolest'],
    title: 'Product 6',
    content: 'This is Product 6',
    price: 60,
  },
];

export default data;
