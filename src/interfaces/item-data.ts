export interface ItemData {
  productId: number;
  options: string[];
  title: string;
  content: string;
  price: number;
}

export interface BasketItem {
  price: number;
  index: number;
  quantity: number;
  title: string;
  options: string[];
}
