/* eslint-disable react/jsx-indent */
import React from 'react';
import { Button } from '../button';
import { BasketItem } from '../../interfaces/index';
import './basket.scss';

interface BasketProps {
  className?: string;
  onCheckout?: (event: React.MouseEvent) => void;
  basket: BasketItem[];
}

const BasketItems: React.FC<{ basket: BasketItem[] }> = ({ basket }) => {
  const totalCost = () => {
    let cost = 0;

    basket.forEach(basketItem => {
      cost += basketItem.quantity * basketItem.price;
    });

    return cost;
  };

  return (
    <>
      {basket.map((basketItem: BasketItem) => (
        <>
          {basketItem.quantity} x {basketItem.title} HK${basketItem.price}
          {basketItem.options.length > 0 ? <br /> : ''}
          {basketItem.options.length > 0 ? '(' : ''}
          {basketItem.options.map((option: string, index: number) => (
            <>
              {option}
              {index < basketItem.options.length - 1 ? ',' : ''}
            </>
          ))}
          {basketItem.options.length > 0 ? ')' : ''}
          <br /> <br />
        </>
      ))}
      <div className="basket__content">Total Cost: {totalCost()}</div>
    </>
  );
};

export const Basket: React.FC<BasketProps> = ({ basket, className, onCheckout, ...props }) => {
  return (
    <div className={`basket ${className}`} {...props}>
      <div className="basket__heading">Your Basket</div>
      {basket.length > 0 ? <BasketItems basket={basket} /> : 'Your basket is empty...'}

      <Button
        className="basket__checkout"
        variant={basket.length > 0 ? 'secondary' : 'disabled'}
        onClick={onCheckout}
      >
        Checkout
      </Button>
    </div>
  );
};
