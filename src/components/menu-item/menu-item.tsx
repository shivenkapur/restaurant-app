import React from 'react';
import { Button } from '../button';
import coffee from './images/coffee.png';
import { MenuItemOptions } from './menu-item-options';
import { MenuItemQuantity } from './menu-item-quantity';
import { ItemData } from '../../interfaces/index';
import './menu-item.scss';

export interface MenuItemProps {
  variant?: 'basic' | 'extended';
  className?: string;
  onOptionSelected?: (selectedOptions: string[]) => void;
  onQuantitySelected?: (quantity: number) => void;
  onCancel?: (event: React.MouseEvent) => void;
  onAddOrder?: (event: React.MouseEvent) => void;
  reset?: boolean;
  itemData?: ItemData;
}

export const MenuItem: React.FC<MenuItemProps> = ({
  className,
  variant,
  onOptionSelected,
  onQuantitySelected,
  onCancel,
  onAddOrder,
  reset,
  itemData,
  ...props
}) => {
  return (
    <div className={`menu-item ${className}`} {...props}>
      <img alt="coffee" className="menu-item__image" src={coffee} />
      <div className="menu-item__heading">
        {itemData?.title} (Price: ${itemData?.price})
      </div>
      <div className="menu-item__content">{itemData?.content}</div>

      {variant === 'extended' ? (
        <>
          <MenuItemQuantity reset={reset} onQuantitySelected={onQuantitySelected} />
          <MenuItemOptions
            options={itemData ? itemData.options : []}
            reset={reset}
            onOptionSelected={onOptionSelected}
          />
        </>
      ) : undefined}

      <div className="menu-item__button-rack">
        <Button
          className={`menu-item__cancel menu-item__cancel-${variant}`}
          variant="plain"
          onClick={onCancel}
        >
          Cancel
        </Button>
        <Button
          className={`menu-item__add-order menu-item__add-order-${variant}`}
          onClick={onAddOrder}
        >
          Add to order
        </Button>
      </div>
    </div>
  );
};

MenuItem.defaultProps = {
  variant: 'basic',
};
