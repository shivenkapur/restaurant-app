/* eslint-disable jsx-a11y/no-onchange */
import React from 'react';

interface MenuItemQuantityProps {
  onQuantitySelected?: (quantity: number) => void;
  reset?: boolean;
}

export const MenuItemQuantity: React.FC<MenuItemQuantityProps> = ({
  onQuantitySelected,
  reset,
}) => {
  const [selectedValue, setSelectedValue] = React.useState<number>(1);

  const handleChange = (event: React.FocusEvent<HTMLSelectElement>) => {
    setSelectedValue(parseInt(event.target.value, 10));
    if (onQuantitySelected) onQuantitySelected(parseInt(event.target.value, 10));
  };

  React.useEffect(() => {
    setSelectedValue(1);
  }, [reset]);

  return (
    <>
      <select className="menu-item__quantity" value={selectedValue} onChange={handleChange}>
        {Array.from(Array(10)).map((value, index) => (
          <option key={value} value={index + 1}>
            {index + 1}
          </option>
        ))}
      </select>
    </>
  );
};
