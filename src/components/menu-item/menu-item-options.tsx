import React from 'react';

interface MenuItemOptionsProps {
  options: string[];
  onOptionSelected?: (selectedOptions: string[]) => void;
  reset?: boolean;
}

export const MenuItemOptions: React.FC<MenuItemOptionsProps> = ({
  reset,
  onOptionSelected,
  options,
}) => {
  const [selectedOptions, setSelectedOptions] = React.useState<string[]>([]);
  const labelClicked = (option: string) => {
    let optionIndex = -1;

    let newSelectedOptions = selectedOptions.filter((selectedOption, selectedOptionIndex) => {
      if (option === selectedOption) {
        optionIndex = selectedOptionIndex;

        return false;
      }

      return true;
    });

    if (optionIndex === -1) newSelectedOptions = [...selectedOptions, option];

    setSelectedOptions(newSelectedOptions);

    if (onOptionSelected) onOptionSelected(newSelectedOptions);
  };

  React.useEffect(() => {
    setSelectedOptions([]);
  }, [reset]);

  return (
    <div className="menu-item__options">
      {options.map((option: string) => (
        <label key={option} htmlFor={option}>
          <input type="checkbox" onClick={() => labelClicked(option)} />
          {option}
        </label>
      ))}
    </div>
  );
};
