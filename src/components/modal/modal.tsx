import React from 'react';
import './modal.scss';

export interface ModalProps {
  visible?: boolean;
}

function useLockBodyScroll() {
  React.useLayoutEffect(() => {
    const originalStyle = window.getComputedStyle(document.body).overflow;

    document.body.style.overflow = 'hidden';

    return () => {
      document.body.style.overflow = originalStyle;
    };
  }, []); // Empty array ensures effect is only run on mount and unmount
}

export const Modal: React.FC<ModalProps> = ({ visible, children, ...props }) => {
  useLockBodyScroll();

  return (
    <div className={`modal ${!visible ? 'modal--hidden' : ''}`} {...props}>
      <div className="modal__card">{children}</div>
    </div>
  );
};
