import React from 'react';
import './button.scss';

export interface ButtonProps {
  variant?: 'primary' | 'secondary' | 'plain' | 'disabled';
  className?: string;
  onClick?: (event: React.MouseEvent) => void;
}
export const Button: React.FC<ButtonProps> = ({ className, variant, children, ...props }) => {
  return (
    <button className={`button button--${variant} ${className}`} type="button" {...props}>
      {children}
    </button>
  );
};

Button.defaultProps = {
  variant: 'primary',
};
