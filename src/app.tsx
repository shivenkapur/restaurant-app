/* eslint-disable unicorn/filename-case */
import React from 'react';
import { RestaurantPage } from './pages/restaurant-page/index';
import './theme/global.scss';

function App() {
  return <RestaurantPage />;
}

export default App;
