import React from 'react';
import { MenuItem, Modal } from '../../components/index';
import { BasketItem } from '../../interfaces/index';
import menuItems from '../../data';

const basketItemInitial: BasketItem = {
  quantity: 1,
  options: [],
  price: 0,
  index: -1,
  title: '',
};

export const RestaurantMenuList: React.FC<{ onBasketChange: (basket: BasketItem[]) => void }> = ({
  onBasketChange,
}) => {
  const [selectedIndex, setSelectedIndex] = React.useState<number>(-1);
  const [basketItem, setBasketItem] = React.useState<BasketItem>(basketItemInitial);
  const [basket, setBasket] = React.useState<BasketItem[]>([]);

  const addToBasket = () => {
    const newBasketItem: BasketItem = {
      ...basketItem,
      price: menuItems[selectedIndex].price,
      title: menuItems[selectedIndex].title,
    };

    setBasket([...basket, newBasketItem]);
    onBasketChange([...basket, newBasketItem]);

    setSelectedIndex(-1);
    setBasketItem(basketItemInitial);
  };

  return (
    <>
      <div className="restaurant__menu-list">
        {menuItems.map((menuItem, index: number) => (
          <MenuItem
            key={menuItem.productId}
            className="restaurant__menu-list-item"
            itemData={menuItems[index]}
            onAddOrder={() => setSelectedIndex(index)}
          />
        ))}
      </div>

      <Modal visible={selectedIndex !== -1}>
        <MenuItem
          itemData={selectedIndex !== -1 ? menuItems[selectedIndex] : undefined}
          reset={selectedIndex !== -1}
          variant="extended"
          onAddOrder={addToBasket}
          onCancel={() => setSelectedIndex(-1)}
          onOptionSelected={optionsSelected => {
            setBasketItem({ ...basketItem, options: optionsSelected });
          }}
          onQuantitySelected={quantity => setBasketItem({ ...basketItem, quantity })}
        />
      </Modal>
    </>
  );
};
