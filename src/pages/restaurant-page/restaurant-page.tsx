import React from 'react';
import { Basket } from '../../components/index';
import { RestaurantMenuList } from './restaurant-menu-list';
import { BasketItem } from '../../interfaces/index';
import './restaurant-page.scss';

const content = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit involuptate velit esse cillum dolore eu fugiat
nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
officia deserunt mollit anim id est laborum.`;

export const RestaurantPage: React.FC = () => {
  const [basket, setBasket] = React.useState<BasketItem[]>([]);

  return (
    <div className="restaurant">
      <div className="restaurant__menu">
        <div className="restaurant__heading">Restaurant App</div>
        <p className="restaurant__content">{content}</p>

        <RestaurantMenuList onBasketChange={setBasket} />
      </div>
      <div className="restaurant__basket">
        <Basket basket={basket} />
      </div>
    </div>
  );
};
