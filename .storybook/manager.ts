import { create } from '@storybook/theming/create';
import { addons } from '@storybook/addons';

const theme = create({
  base: 'light',

  colorSecondary: '#fbbc00',

  appBg: '#F8F8F8',
  appBorderColor: '#EDEDED',
  appBorderRadius: 6,

  barTextColor: '#999999',
  barSelectedColor: '#fbbc00',
  barBg: '#F2F2F2',

  inputBg: 'white',
  inputBorder: 'rgba(0,0,0,.1)',
  inputTextColor: '#333333',
  inputBorderRadius: 4,

  brandTitle: 'Network Guard',
  brandUrl: 'https://networkguard.com/',
  brandImage: 'network-guard-logo.png',
});

addons.setConfig({
  theme,
  panelPosition: 'right',
});
